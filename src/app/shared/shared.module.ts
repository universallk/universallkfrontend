import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {SharedRoutingModule} from './shared-routing.module';
import {AuthorityDirective} from './directive/authority.directive';
import {BooleanPipe} from './models/pipe/boolean.pipe';
import {VklOtklPipe} from './models/pipe/vkl-otkl.pipe';
import { KontursPipe } from './models/pipe/konturs.pipe';
import {BooleanNamePipe} from './models/pipe/boolean-name.pipe';
import {AppTypePipe} from './models/pipe/app-type.pipe';
import {AppTypeNamePipe} from './models/pipe/app-tyme-name.pipe';
import { SoundMessagePipe } from './models/pipe/sound-message.pipe';


@NgModule({
  declarations: [AuthorityDirective, BooleanPipe,BooleanNamePipe, VklOtklPipe, KontursPipe, AppTypePipe, AppTypeNamePipe, SoundMessagePipe],
  imports: [
    CommonModule,
    SharedRoutingModule
  ],
    exports: [AuthorityDirective, BooleanPipe, VklOtklPipe, KontursPipe, AppTypeNamePipe, AppTypePipe, SoundMessagePipe
    ]
})
export class SharedModule {
}
