export class Skill {
  constructor(
    public id?: number,
    public name?: string,
    public value?: string,
    public appId?: number
  ) {
  }
}
