import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'appType'
})
export class AppTypePipe implements PipeTransform {
  private appType: any = {
  IVR: {name: 'badge badge-danger'},
  INTERVIEW: {name: 'badge badge-primary'},
  };

  transform(value: string): string {
    return this.appType[value].name;
  }

}
