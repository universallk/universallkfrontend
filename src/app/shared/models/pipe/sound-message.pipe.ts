import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'soundMessage'
})
export class SoundMessagePipe implements PipeTransform {
  private authorities: any = {
    no_sounded_message: {name: 'НЕ ОЗВУЧЕНО'},
    sounded_message: {name: 'ОЗВУЧЕНО'},
  };

  transform(value: string): string {
    return this.authorities[value].name;
  }
}
