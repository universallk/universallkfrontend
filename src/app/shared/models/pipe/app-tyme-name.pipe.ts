import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'appTypeName'
})
export class AppTypeNamePipe implements PipeTransform {
  private appType: any = {
    IVR: {name: 'IVR'},
    INTERVIEW: {name: 'ОПРОСНИК'},
  };

  transform(value: string): string {
    return this.appType[value].name;
  }
}
