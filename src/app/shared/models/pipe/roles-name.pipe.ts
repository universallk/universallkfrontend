import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'rolesName'
})
export class RolesNamePipe implements PipeTransform {
  private rolesName: any = {
    ADMIN: {name: 'Администратор'},
    USER: {name: 'Пользователь'},
    USER_IVR: {name: 'Пользователь_IVR'},
    SUPER_VISOR: {name: 'SuperVisor'}
  };

  transform(value: string): string {
    return this.rolesName[value].name;
  }
}
