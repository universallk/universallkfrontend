import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'roles'
})
export class RolesPipe implements PipeTransform {
  private authorities: any = {
    ADMIN: {name: 'badge badge-danger'},
    USER: {name: 'badge badge-success'},
    USER_IVR: {name: 'badge badge-secondary'},
    SUPER_VISOR: {name: 'badge badge-primary'},
  };

  transform(value: string): string {
    return this.authorities[value].name;
  }

}
