import {Pipe, PipeTransform} from '@angular/core';
import {Kontur} from '../kontur.model';
import {KonturToString} from '../kontur-to-string.model';

@Pipe({
  name: 'konturs'
})
export class KontursPipe implements PipeTransform {
  private authorities: any = {
    false: {name: 'Нет'},
    true: {name: 'Да'},
  };

  transform(value: Kontur[]): string {
    const konturToString: KonturToString[] = [];
    if (value && value.length) {
      value.forEach(e=> konturToString.push(new KonturToString(e.name, e.konturNumber)));
      return JSON.stringify(konturToString);
    } else {
      return '';
    }
  }
}
