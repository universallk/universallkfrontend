import {Kontur} from './kontur.model';

export interface IApp {
  id?: number;
  name?: string;
  errorMessage?: string;
  ip?: string;
  konturs?: Kontur[];
  remove?: number[];
  dataBase?: string;
  login?: string;
  password?: string;
  enabled?: boolean;
  appType?: string;
}

export class App implements IApp {
  constructor(
    public id?: number,
    public name?: string,
    public errorMessage?: string,
    public konturs?: Kontur[],
    public remove?: number[],
    public ip?: string,
    public dataBase?: string,
    public login?: string,
    public password?: string,
    public enabled?: boolean,
    public appType?: string,
  ) {
    this.enabled = this.enabled || false;
  }
}
