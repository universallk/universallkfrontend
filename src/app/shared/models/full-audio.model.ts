import {Moment} from 'moment';

export class FullAudio  {
  constructor(
    public id?: number,
    public appName?: string,
    public appId?: number,
    public phone?: string,
    public connectDate?: string,
    public recordFile?: string,
    public  theme?: string,
    public  extradata?: string,
    public  skill?: string,
    public  disconectAction?: string,
    public  soundMessage?: string,
    public  sp_004_v3?: string,
    public  timeDiff?: string,
  ) {
  }
}
