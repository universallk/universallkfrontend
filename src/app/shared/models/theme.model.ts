export class Theme {
  constructor(
    public id?: number,
    public name?: string,
    public value?: string,
    public appId?: number
  ) {
  }
}
