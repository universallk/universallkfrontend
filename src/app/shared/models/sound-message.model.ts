export class SoundMessage {
  constructor(
    public name?: string,
    public value?: string,
  ) {
  }
}
