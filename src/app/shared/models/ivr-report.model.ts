import {Moment} from 'moment';

export class IvrReport {
  constructor(
    public themeName?: string,
    public hourValue?: string[],
    public isBlack?: boolean,
  ) {
  }
}
