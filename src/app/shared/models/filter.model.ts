import {Moment} from 'moment';

export class Filter {
  constructor(
    public id?: number,
    public phone?: string,
    public theme?: string,
    public skill?: string,
    public soundMessage?: string,
    public isCount?: number,
    public dateStart?: Moment,
    public dateEnd?: Moment,
  ) {
  }
}
