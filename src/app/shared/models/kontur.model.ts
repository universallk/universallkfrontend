export class Kontur {
  constructor(
    public id?: number,
    public name?: string,
    public konturNumber?: number,
    public app?: number,
  ) {
  }
}
