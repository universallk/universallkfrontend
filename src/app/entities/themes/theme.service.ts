import { Injectable } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthService} from '../../core/auth/auth.service';
import {Observable} from 'rxjs';
import {Page} from '../../shared/models/page.model';
import {Theme} from '../../shared/models/theme.model';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/themes';
  constructor(private http: HttpClient, protected authService: AuthService) { }


  findAll(options: HttpParams): Observable<HttpResponse<Page<Theme>>> {
    return this.http.get<Page<Theme>>(this.url, {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

  findAllNotPage(options: HttpParams): Observable<HttpResponse<Theme[]>> {
    return this.http.get<Theme[]>(this.url + '/non-page', {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }
  save(options: Theme): Observable<HttpResponse<Theme>> {
    return this.http.post<Theme>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  update(options: Theme): Observable<HttpResponse<Theme>> {
    return this.http.put<Theme>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.url}/${id}`,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }
}
