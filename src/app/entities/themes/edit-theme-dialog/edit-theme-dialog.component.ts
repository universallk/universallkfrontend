import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ThemeService} from '../theme.service';
import {Theme} from '../../../shared/models/theme.model';
import {App, IApp} from '../../../shared/models/app.model';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';

@Component({
  selector: 'app-edit-theme-dialog',
  templateUrl: './edit-theme-dialog.component.html',
  styleUrls: ['./edit-theme-dialog.component.scss']
})
export class EditThemeDialogComponent implements OnInit {
  appId: number;
  themeService: ThemeService;
  theme: Theme;
  errorMessage?: string;

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, themeService: ThemeService) {
    this.themeService = themeService;
  }

  editForm = this.fb.group({
    id: [null],
    name: [null, [Validators.required]],
    value: [null, [Validators.required]],
    appId: [null, [Validators.required]],
  });
  ngOnInit(): void {
  }

  clear() {
    this.activeModal.close();
  }
  save() {
    const theme1 = this.updateApp();
    console.log('сохраняем приложение');
    if (!theme1.id) {
      this.themeService.save(theme1).subscribe(res => {
          this.saveResult(res);
        }
      );
    } else {
      this.themeService.update(theme1).subscribe(res => {
        this.saveResult(res);
      });
    }
  }

  private saveResult(res: HttpResponse<IApp>) {
    console.log(res.body.id);
    if (res.body && !res.body.errorMessage) {
      this.activeModal.close({
        save: true
      });
    } else if (res.body && res.body.errorMessage){
      this.errorMessage = res.body.errorMessage;
    }
  }

  updateApp(): Theme {
    return {
      ...new Theme(),
      id: this.editForm.get(['id']).value ? this.editForm.get(['id']).value : null,
      name: this.editForm.get(['name']).value,
      value: this.editForm.get(['value']).value,
      appId: this.editForm.get(['appId']).value,
    };
  }

  init() {
    console.log(this.theme);
    if (this.theme) {
      this.editForm.patchValue({
        id: this.theme.id,
        name: this.theme.name,
        value: this.theme.value,
        appId: this.theme.appId,
      });
    } else {
      this.editForm.patchValue({
        appId: this.appId
      });
    }
  }

}
