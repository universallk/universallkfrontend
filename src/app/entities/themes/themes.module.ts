import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ThemesRoutingModule } from './themes-routing.module';
import { ThemesComponent } from './themes.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import { EditThemeDialogComponent } from './edit-theme-dialog/edit-theme-dialog.component';
import {AdminModule} from '../../admin/admin.module';
import { DeleteThemeComponent } from './delete-theme/delete-theme.component';


@NgModule({
  declarations: [ThemesComponent, EditThemeDialogComponent, DeleteThemeComponent],
  imports: [
    CommonModule,
    ThemesRoutingModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    AdminModule,
    NgbModule,
    FormsModule,
  ],
  entryComponents: [EditThemeDialogComponent, DeleteThemeComponent]
})
export class ThemesModule { }
