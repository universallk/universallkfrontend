import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {ThemesComponent} from './themes.component';

const routes: Routes = [
  {
    path: 'themes',
    component: ThemesComponent,
    data: {
      authorities: ['ADMIN'],
    },
    canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ThemesRoutingModule { }
