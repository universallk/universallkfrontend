import {Component, OnInit} from '@angular/core';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {EventManagerService} from '../../shared/sevice/event-manager.service';
import {AppsService} from '../apps/apps.service';
import {ThemeService} from './theme.service';
import {Theme} from '../../shared/models/theme.model';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../core/auth/auth.service';
import {IApp} from '../../shared/models/app.model';
import {HttpParams} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DeleteAppComponent} from '../apps/delete-app/delete-app.component';
import {EditThemeDialogComponent} from './edit-theme-dialog/edit-theme-dialog.component';
import {DeleteThemeComponent} from './delete-theme/delete-theme.component';

@Component({
  selector: 'app-themes',
  templateUrl: './themes.component.html',
  styleUrls: ['./themes.component.scss']
})
export class ThemesComponent implements OnInit {
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  eventManagerService: EventManagerService;
  appsService: AppsService;
  themeService: ThemeService;
  themes: Theme[] = [];
  modalService: NgbModal;

  filterForm = this.fb.group({
    appId: [null, [Validators.required]],
  });
  apps: IApp[] = [];


  constructor(modalService: NgbModal, protected fb: FormBuilder,
              authService: AuthService, appService: AppsService, themeService: ThemeService) {
    this.appsService = appService;
    this.modalService = modalService;
    this.themeService =themeService;
  }

  ngOnInit(): void {
    this.loadApp();
  }

  trackAppById(index: number, item: IApp) {
    return item.id;
  }

  addNewTheme() {
    const modalRef = this.modalService.open(EditThemeDialogComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.appId = this.filterForm.get(['appId']).value;
    modalRef.componentInstance.init();
    modalRef.result.then(result => {
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  loadApp() {
    let options: HttpParams = new HttpParams();
    options = options.set('isEnabled.equals', 'true');
    options = options.set('appType.equals', 'IVR');
    this.appsService.findAllNotDisabled(options).subscribe(res => {
      this.apps = res.body;
    });
  }

  loadPage(page?: number) {
    const pageToLoad: number = page ? page : this.page;
    console.log(pageToLoad);
    let options: HttpParams = new HttpParams();
    if (pageToLoad !== undefined) {
      options = options.set('page', (pageToLoad - 1).toString());
    }
    options = options.set('size', this.itemsPerPage.toString());
    options = options.set('sort', 'id');

    if (this.filterForm.get(['appId']).value) {
      options = options.set('appId.equals',  this.filterForm.get(['appId']).value);
    }
    this.themeService.findAll(options).subscribe(res => {
      this.themes = res.body.content;
      this.totalItems = res.body.totalElements;
    });
  }

  edit(theme: Theme) {
    const modalRef = this.modalService.open(EditThemeDialogComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.theme = theme;
    modalRef.componentInstance.init();
    modalRef.result.then(result => {
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  delete(theme: Theme): void {
    const modalRef = this.modalService.open(DeleteThemeComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.theme = theme;
    modalRef.result.then(result => {
      console.log(result);
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  changeApp() {
    if (!this.filterForm.invalid && this.filterForm.get(['appId']).value) {
      this.loadPage(1);
    } else {
      this.themes = [];
    }
  }
}
