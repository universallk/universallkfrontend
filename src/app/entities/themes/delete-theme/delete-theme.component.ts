import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Theme} from '../../../shared/models/theme.model';
import {ThemeService} from '../theme.service';
@Component({
  selector: 'app-delete-theme',
  templateUrl: './delete-theme.component.html',
  styleUrls: ['./delete-theme.component.scss']
})
export class DeleteThemeComponent implements OnInit {
  theme?: Theme;
  constructor(
    protected themeService: ThemeService,
    public activeModal: NgbActiveModal,
  ) {
  }

  clear(): void {
    this.activeModal.close();
  }

  confirmDelete(id: number): void {
    this.themeService.delete(id).subscribe(() => {
      this.activeModal.close({
        save: true
      });
    });
  }

  ngOnInit(): void {
  }

}
