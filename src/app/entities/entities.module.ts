import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {UserModule} from './user/user.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {AppsModule} from './apps/apps.module';
import {InterviewModule} from './interview/interview.module';
import {ThemesModule} from './themes/themes.module';
import {SkillsComponent} from './skills/skills.component';
import {SkillsModule} from './skills/skills.module';
import {IvrModule} from './ivr/ivr.module';
import {ReportsModule} from './reports/reports.module';

const routes: Routes = [
  {
    path: 'userList',
    loadChildren: () => import('./user/user.module').then(m => m.UserModule)
  },
  {
    path: 'apps',
    loadChildren: () => import('./apps/apps.module').then(m => m.AppsModule)
  },
  {
    path: 'appsInterview',
    loadChildren: () => import('./interview/interview.module').then(m => m.InterviewModule)
  },
  {
    path: 'appsIvr',
    loadChildren: () => import('./ivr/ivr.module').then(m => m.IvrModule)
  },
  {
    path: 'reports',
    loadChildren: () => import('./reports/reports.module').then(m => m.ReportsModule)
  },
  {
    path: 'themes',
    loadChildren: () => import('./themes/themes.module').then(m => m.ThemesModule)
  },
  {
    path: 'skills',
    loadChildren: () => import('./skills/skills.module').then(m => m.SkillsModule)
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes),
    CommonModule, UserModule, NgbPaginationModule, AppsModule, InterviewModule, ThemesModule, SkillsModule, IvrModule, ReportsModule
  ]
})
export class EntitiesModule { }
