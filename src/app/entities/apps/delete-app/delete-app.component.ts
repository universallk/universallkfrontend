import { Component, OnInit } from '@angular/core';
import {IApp} from '../../../shared/models/app.model';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AppsService} from '../apps.service';

@Component({
  selector: 'app-delete-app',
  templateUrl: './delete-app.component.html',
  styleUrls: ['./delete-app.component.scss']
})
export class DeleteAppComponent implements OnInit {
  app?: IApp;

  constructor(
    protected appsService: AppsService,
    public activeModal: NgbActiveModal,
  ) {
  }

  clear(): void {
    this.activeModal.close();
  }

  confirmDelete(id: number): void {
    this.appsService.delete(id).subscribe(() => {
      this.activeModal.close({
        update: true
      });
    });
  }

  ngOnInit(): void {
  }

}

