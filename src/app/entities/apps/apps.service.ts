import { Injectable } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthService} from '../../core/auth/auth.service';
import {Observable} from 'rxjs';
import {Page} from '../../shared/models/page.model';
import {App, IApp} from '../../shared/models/app.model';
import {Kontur} from '../../shared/models/kontur.model';

@Injectable({
  providedIn: 'root'
})
export class AppsService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/apps';
  private urlKontur: string = GLOBAL_URL + '/api/kontur';

  constructor(private http: HttpClient, protected authService: AuthService) {
  }

  findAll(options: HttpParams): Observable<HttpResponse<Page<IApp>>> {
    return this.http.get<Page<IApp>>(this.url, {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

  findAllNotDisabled(options: HttpParams): Observable<HttpResponse<IApp[]>> {
    return this.http.get<IApp[]>(this.url + '/enable', {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

  save(options: IApp): Observable<HttpResponse<IApp>> {
    return this.http.post<IApp>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  update(options: App): Observable<HttpResponse<IApp>> {
    return this.http.put<IApp>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.url}/${id}`,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }
  deleteKontur(id: Kontur[]): Observable<HttpResponse<{}>> {
    return this.http.post(this.urlKontur + '/deleteAllKonturs', id,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }
}
