import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AppsRoutingModule} from './apps-routing.module';
import {AppsComponent} from './apps.component';
import {AppsEditComponent} from './apps-edit/apps-edit.component';
import {AppsFilterComponent} from './apps-filter/apps-filter.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminModule} from '../../admin/admin.module';
import {AppModule} from '../../app.module';
import {NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {SharedModule} from '../../shared/shared.module';
import { DeleteAppComponent } from './delete-app/delete-app.component';
import { AddKonturComponent } from './apps-edit/add-kontur/add-kontur.component';


@NgModule({
  declarations: [AppsComponent, AppsEditComponent, AppsFilterComponent, DeleteAppComponent, AddKonturComponent],
  imports: [
    CommonModule,
    AppsRoutingModule,
    ReactiveFormsModule,
    AdminModule,
    NgbPaginationModule,
    SharedModule,
    NgbModule,
    FormsModule
  ],
  entryComponents: [AppsEditComponent, AppsFilterComponent, DeleteAppComponent, AddKonturComponent]
})
export class AppsModule {
}
