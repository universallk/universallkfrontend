import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {AppsComponent} from './apps.component';


const routes: Routes = [
  {
    path: 'apps',
    component: AppsComponent,
    data: {
      authorities: ['ADMIN'],
    },
    canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppsRoutingModule {
}
