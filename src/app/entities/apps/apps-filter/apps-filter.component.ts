import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {HttpResponse} from '@angular/common/http';
import {IApp} from '../../../shared/models/app.model';

@Component({
  selector: 'app-apps-filter',
  templateUrl: './apps-filter.component.html',
  styleUrls: ['./apps-filter.component.scss']
})
export class AppsFilterComponent implements OnInit {
  nameFilter: string;
  dataBaseFilter: string;

  constructor(public activeModal: NgbActiveModal) {
  }

  ngOnInit() {
  }

  init() {

  }

  trackById(index: number, item: IApp): any {
    return item.id;
  }

  close() {
    this.activeModal.close();
  }

  filter() {
    this.activeModal.close({
      nameFilter: this.nameFilter,
      dataBaseFilter: this.dataBaseFilter,
    });
  }
}
