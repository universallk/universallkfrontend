import {Component, OnInit} from '@angular/core';
import {App, IApp} from '../../shared/models/app.model';
import {HttpParams} from '@angular/common/http';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {AppsService} from './apps.service';
import {AuthService} from '../../core/auth/auth.service';
import {EventManagerService} from '../../shared/sevice/event-manager.service';
import {AppsEditComponent} from './apps-edit/apps-edit.component';
import {AppsFilterComponent} from './apps-filter/apps-filter.component';
import {DeleteAppComponent} from './delete-app/delete-app.component';

@Component({
  selector: 'app-apps',
  templateUrl: './apps.component.html',
  styleUrls: ['./apps.component.scss']
})
export class AppsComponent implements OnInit {
  apps?: IApp[];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  eventManagerService: EventManagerService;
  appsService: AppsService;
  dataBaseFilter: string;
  nameFilter: string;
  modalService: NgbModal;

  constructor(eventManagerService: EventManagerService,
              appsService: AppsService, modalService: NgbModal,
              public authService: AuthService) {
    this.eventManagerService = eventManagerService;
    this.appsService = appsService;
    this.modalService = modalService;
  }

  trackById(index: number, item: IApp): any {
    return item.id;
  }

  ngOnInit() {
    console.log('работает');
    this.registerChangeInApps();
    this.loadPage();
  }

  registerChangeInApps(): void {

    this.eventManagerService.map$.subscribe((map: Map<any, any>) => {
      if (map.has('modifyApp')) {
        console.log('сработала подписка');
        this.loadPage(0);
      }
    });
  }

  loadPage(page?: number) {
    const pageToLoad: number = page ? page : this.page;
    console.log(pageToLoad);
    let options: HttpParams = new HttpParams();
    if (pageToLoad !== undefined) {
      options = options.set('page', (pageToLoad - 1).toString());
    }
    options = options.set('size', this.itemsPerPage.toString());
    options = options.set('sort', 'id');
    if (this.nameFilter) {
      options = options.set('name.equals', this.nameFilter);
    }
    if (this.dataBaseFilter) {
      options = options.set('dataBase.equals', this.dataBaseFilter.toString());
    }
    this.appsService.findAll(options).subscribe(res => {
      this.apps = res.body.content;
      this.totalItems = res.body.totalElements;
    });
  }

  previousState(): void {
    window.history.back();
  }

  trackId(index: number, item: IApp): number {
    // tslint:disable-next-line:no-non-null-assertion
    return item.id!;
  }

  delete(app: IApp): void {
    const modalRef = this.modalService.open(DeleteAppComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.app = app;
    modalRef.result.then(result => {
      console.log(result);
      if (result && result.update) {
        this.loadPage();
      }
    });
  }

  deleteFilters() {
    this.nameFilter = undefined;
    this.dataBaseFilter = undefined;
    this.loadPage(1);
  }

  showFilter() {
    console.log('сработало');
    const modelRef = this.modalService.open(AppsFilterComponent, {size: 'lg', backdrop: 'static'});
    modelRef.componentInstance.dataBaseFilter = this.dataBaseFilter;
    modelRef.componentInstance.nameFilter = this.nameFilter;
    modelRef.componentInstance.init();
    modelRef.result.then(result => {
      if (result) {
        this.nameFilter = result.nameFilter;
        this.dataBaseFilter = result.dataBaseFilter;
        this.loadPage(1);
      }
    });
  }

  edit(app: IApp) {
    const modelRef = this.modalService.open(AppsEditComponent, {size: 'lg', backdrop: 'static'});
    modelRef.componentInstance.app = {...app};
    modelRef.componentInstance.init();
  }

  addNewApp() {
    this.edit(new App());
  }

  getString(b: boolean): string {
    return b.toString();
  }
}
