import { Component, OnInit } from '@angular/core';
import {Kontur} from '../../../../shared/models/kontur.model';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-add-kontur',
  templateUrl: './add-kontur.component.html',
  styleUrls: ['./add-kontur.component.scss']
})
export class AddKonturComponent implements OnInit {
  kontur: Kontur;

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder) {
  }

  editForm = this.fb.group({
    id: [null],
    name: [null, [Validators.required]],
    konturId: [null, [Validators.required]],
  });

  ngOnInit() {
  }

  close() {
    this.activeModal.close();
  }

  init() {
    if (this.kontur) {
      this.editForm.patchValue({
        id: this.kontur.id,
        name: this.kontur.name,
        konturId: this.kontur.konturNumber,
      });
    }

  }

  save() {
    this.kontur = this.updateModel();
    this.activeModal.close({
      kontur: this.kontur,
    });
  }

  updateModel(): Kontur {
    return {
      ...new Kontur(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      konturNumber: this.editForm.get(['konturId']).value,
    };
  }

  clear() {
    this.activeModal.close();
  }
}
