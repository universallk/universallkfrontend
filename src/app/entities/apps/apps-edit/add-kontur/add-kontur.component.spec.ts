import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddKonturComponent } from './add-kontur.component';

describe('AddKonturComponent', () => {
  let component: AddKonturComponent;
  let fixture: ComponentFixture<AddKonturComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddKonturComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddKonturComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
