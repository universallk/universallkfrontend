import {Component, OnInit} from '@angular/core';
import {App, IApp} from '../../../shared/models/app.model';
import {EventManagerService} from '../../../shared/sevice/event-manager.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import {AppsService} from '../apps.service';
import {HttpResponse} from '@angular/common/http';
import {Kontur} from '../../../shared/models/kontur.model';
import {AddKonturComponent} from './add-kontur/add-kontur.component';
import {APP_TYPE} from '../../../shared/constant/app-type.constant';

@Component({
  selector: 'app-apps-edit',
  templateUrl: './apps-edit.component.html',
  styleUrls: ['./apps-edit.component.scss']
})
export class AppsEditComponent implements OnInit {
  app: IApp = new App();

  isSaving = false;
  appsService: AppsService;
  eventManagerService: EventManagerService;
  errorMessage: string | undefined;
  currentKonturs: Kontur[] = [];
  removeKonturs: number[] = [];
  appTypeList = APP_TYPE;
  constructor(public activeModal: NgbActiveModal, appsService: AppsService,
              private fb: FormBuilder, eventManagerService: EventManagerService, public modalService: NgbModal) {
    this.appsService = appsService;
    this.eventManagerService = eventManagerService;
  }

  editForm = this.fb.group({
    id: [null],
    name: [null, [Validators.required]],
    appType: [null, [Validators.required]],
    ip: [null, [Validators.required]],
    dataBase: [null, [Validators.required]],
    login: [null, [Validators.required]],
    password: [null, [Validators.required]],
    isEnabled: [false, [Validators.required]],
  });

  ngOnInit() {

  }


  clear() {
    this.activeModal.close();
  }

  save() {
    this.isSaving = true;
    const appRes = this.updateApp();
    console.log('сохраняем приложение');
    console.log(appRes);
    if (!appRes.id) {
      this.appsService.save(appRes).subscribe(res => {
          this.saveResult(res);
        }
      );
    } else {
      console.log('удаляем список контуров = ' + this.removeKonturs);
      appRes.remove= this.removeKonturs;
          this.appsService.update(appRes).subscribe(res => {
            this.saveResult(res);
          });
      }
  }

  init() {
    console.log(this.app);
    if (this.app) {
      this.editForm.patchValue({
        id: this.app.id,
        name: this.app.name,
        appType: this.app.appType,
        ip: this.app.ip,
        dataBase: this.app.dataBase,
        login: this.app.login,
        password: this.app.password,
        isEnabled: this.app.enabled,
      });
      this.currentKonturs = this.app.konturs ? this.app.konturs : [];
    }
  }


  updateApp(): App {
    return {
      ...new App(),
      id: this.editForm.get(['id']).value ? this.editForm.get(['id']).value : null,
      name: this.editForm.get(['name']).value,
      appType: this.editForm.get(['appType']).value,
      errorMessage: null,
      konturs: this.currentKonturs,
      ip: this.editForm.get(['ip']).value,
      dataBase: this.editForm.get(['dataBase']).value,
      login: this.editForm.get(['login']).value,
      password: this.editForm.get(['password']).value,
      enabled: this.editForm.get(['isEnabled']).value,
    };
  }

  trackById(index: number, item: IApp): any {
    return item.id;
  }

  previousState(): void {
    window.history.back();
  }

  private saveResult(res: HttpResponse<IApp>) {
    this.isSaving = false;
    console.log(res.body.id);

    if (res.body && !res.body.errorMessage) {
      this.eventManagerService.send(new Map().set('modifyApp', 'modifyApp'));
      this.clear();
    } else if (res.body && res.body.errorMessage){
      this.errorMessage = res.body.errorMessage;
    }
  }

  setEnabled(): void {
  }

  closeError(): void {
    this.errorMessage = undefined;
  }

  deleteKontur(idex: number) {
    const currentKontursTmp: Kontur[] = [];
    for (let i = 0; i < this.currentKonturs.length; i++) {
      if (i !== idex) {
        currentKontursTmp.push(this.currentKonturs[i]);
      } else {
        if (this.currentKonturs[i].id) {
          console.log('попытка удалить');
          this.removeKonturs.push(this.currentKonturs[i].id);
        }
      }
    }
    this.currentKonturs = currentKontursTmp;
  }

  edit(idex: number, kontur: Kontur) {
    const modelRef = this.modalService.open(AddKonturComponent, {size: 'lg', backdrop: 'static'});
    modelRef.componentInstance.kontur = {...this.currentKonturs[idex]};
    modelRef.componentInstance.init();
    modelRef.result.then(result => {
      console.log(result);
      if (result && result.kontur) {
        this.currentKonturs[idex] = result.kontur;
      }
    });
  }

  addKontur() {
    const modelRef = this.modalService.open(AddKonturComponent, {size: 'lg', backdrop: 'static'});
    modelRef.componentInstance.init();
    modelRef.result.then(result => {
      console.log(result);
      if (result && result.kontur) {
        console.log(this.currentKonturs);
        this.currentKonturs.push(result.kontur);
      }
    });
  }
}
