import { Injectable } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthService} from '../../core/auth/auth.service';
import {Observable} from 'rxjs';
import {Page} from '../../shared/models/page.model';
import {IApp} from '../../shared/models/app.model';
import {IvrReport} from '../../shared/models/ivr-report.model';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/reports';
  constructor(private http: HttpClient, protected authService: AuthService) { }


  createReport(options: HttpParams): Observable<HttpResponse<IvrReport[]>> {
    console.log(this.authService.getCurrentToken());
    return this.http.get<IvrReport[]>(this.url + '/ivr-preview'  , {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }
  download(options: HttpParams): Observable<HttpResponse<Blob>> {
    return this.http.get(`${this.url}/download/`,
      {
        params: options,
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response', responseType: 'blob' });
  }
}
