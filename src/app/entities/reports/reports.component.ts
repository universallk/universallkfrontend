import {Component, OnInit} from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {AppsService} from '../apps/apps.service';
import {IApp} from '../../shared/models/app.model';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {FullAudio} from '../../shared/models/full-audio.model';
import {AuthService} from '../../core/auth/auth.service';
import {ThemeService} from '../themes/theme.service';
import {SkillsService} from '../skills/skills.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {SoundMessage} from '../../shared/models/sound-message.model';
import {SOUND_MESSAGES} from '../../shared/constant/sound-messages.constant';
import {Skill} from '../../shared/models/skill.model';
import {Theme} from '../../shared/models/theme.model';
import {Filter} from '../../shared/models/filter.model';
import * as moment from 'moment';
import {DATE_TIME_FORMAT, DATE_TIME_FORMAT2} from '../../shared/constant/format.constant';
import {ReportsService} from './reports.service';
import {IvrReport} from '../../shared/models/ivr-report.model';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  phoneMask = [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  url: string = GLOBAL_URL + '/api/interview';
  errorMessage?: string = 'Задайте условия поиска';
  appsService: AppsService;
  apps: IApp[] = [];
  reportsService: ReportsService;
  authService: AuthService;
  isSpinner = true;
  isSpinner2 = true;
  themeService: ThemeService;
  skillsService: SkillsService;
  filterCount = 0;
  reportList: IvrReport[];

  constructor(private http: HttpClient, appsService: AppsService, protected router: Router, reportsService: ReportsService,
              protected fb: FormBuilder, authService: AuthService,
              themeService: ThemeService, skillsService: SkillsService) {
    this.appsService = appsService;
    this.authService = authService;
    this.reportsService = reportsService;
    this.skillsService = skillsService;
    this.themeService = themeService;
  }

  filterForm = this.fb.group({
    id: [null, [Validators.required]],
    dateStart: [null, [Validators.required]],
    dateEnd: [null, [Validators.required]],
  });

  ngOnInit(): void {
    this.initCurrentDateTime();
    this.loadEnabledApp();
  }

  loadEnabledApp() {
    let options: HttpParams = new HttpParams();
    options = options.set('isEnabled.equals', 'true');
    options = options.set('appType.equals', 'IVR');
    this.appsService.findAllNotDisabled(options).subscribe(res => {
      this.apps = res.body;
      console.log(this.apps.length);
    });
  }

  previousState() {
    window.history.back();
  }

  trackById(index: number, item: IApp) {
    return item.id;
  }

  clearMessage() {
    this.errorMessage = null;
  }


  loadReports() {
    this.isSpinner = false;
    this.errorMessage = undefined;
    let options: HttpParams = new HttpParams();
    const filter = this.getFilter();
    if (filter.id) {
      options = options.set('id', filter.id.toString());
    }
    if (filter.dateStart && filter.dateStart.isValid()) {
      console.log(filter.dateStart.toJSON());
      options = options.set('dateStart', this.filterForm.get(['dateStart']).value);
    }
    if (filter.dateEnd && filter.dateEnd.isValid()) {
      options = options.set('dateEnd', this.filterForm.get(['dateEnd']).value);
    }
    this.reportsService.createReport(options).subscribe(res => {
      this.isSpinner = true;
      this.reportList = res.body;
    }, error => {
      this.errorMessage = 'Произошла ошибка';
      this.isSpinner = true;
    })
  }

  clearFilter() {
    this.filterForm.patchValue({
      id: null,
      phone: null,
      dateStart: null,
      dateEnd: null,
    });
    this.reportList = [];
  }

  getFilter(): Filter {
    return {
      ...new Filter(),
      id: this.filterForm.get(['id']).value,
      dateStart: moment(this.filterForm.get(['dateStart']).value, DATE_TIME_FORMAT),
      dateEnd: moment(this.filterForm.get(['dateEnd']).value, DATE_TIME_FORMAT),
    };
  }

  initCurrentDateTime(): void {
    this.filterForm.patchValue({
      dateStart: moment().startOf('day').format(DATE_TIME_FORMAT2),
      dateEnd: moment().endOf('day').format(DATE_TIME_FORMAT2)
    });
  }

  changeStatDateTime(): void {
    this.changeCount();
    console.log('сработало');
    if (this.filterForm.get(['dateStart']).value) {
      let datEnd = moment(this.filterForm.get(['dateStart']).value, DATE_TIME_FORMAT);
      if (datEnd && datEnd.isValid()) {
        datEnd.add('days', 1);
        console.log(datEnd.toJSON());
        this.filterForm.patchValue({
          dateEnd: datEnd.format(DATE_TIME_FORMAT2)
        });
      }
    }
  }

  changeApp(): void {
    this.changeCount();
    this.reportList = [];
  }

  changeCount() {
    this.filterCount = 0;
  }

  downLoadReports() {
    this.isSpinner2 = false;
    let options: HttpParams = new HttpParams();
    const filter = this.getFilter();
    if (filter.id) {
      options = options.set('id', filter.id.toString());
    }
    if (filter.dateStart && filter.dateStart.isValid()) {
      console.log(filter.dateStart.toJSON());
      options = options.set('dateStart', this.filterForm.get(['dateStart']).value);
    }
    if (filter.dateEnd && filter.dateEnd.isValid()) {
      options = options.set('dateEnd', this.filterForm.get(['dateEnd']).value);
    }
    this.reportsService.download(options).pipe(
      map(res => {
        console.log(res.headers);
        console.log(res.body);

        return {
          filename: res.headers.get('filename'),
          data: res.body
        };
      })
    ).subscribe(res => {
      this.isSpinner2 = true;
      const url = window.URL.createObjectURL(res.data);

      const a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = url;
      a.download = "onlineReport.xlsx";
      a.click();
      window.URL.revokeObjectURL(url);
      a.remove();
    }, error => {
      this.isSpinner2 = true;
    });
  }
}
