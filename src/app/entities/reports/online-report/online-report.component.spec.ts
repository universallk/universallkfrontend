import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnlineReportComponent } from './online-report.component';

describe('OnlineReportComponent', () => {
  let component: OnlineReportComponent;
  let fixture: ComponentFixture<OnlineReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnlineReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnlineReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
