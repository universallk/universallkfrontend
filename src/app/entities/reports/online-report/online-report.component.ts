import {Component, Input, OnInit} from '@angular/core';
import {IvrReport} from '../../../shared/models/ivr-report.model';
import {ReportsService} from '../reports.service';
import {Filter} from '../../../shared/models/filter.model';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-online-report',
  templateUrl: './online-report.component.html',
  styleUrls: ['./online-report.component.scss']
})
export class OnlineReportComponent implements OnInit {
  @Input() reportList: IvrReport[];
  @Input() appId: number;
  @Input() dateStart: string;
  @Input() dateEnd: string;
  @Input() filter: Filter;

  reportsService: ReportsService;

  constructor(reportsService: ReportsService) {
    this.reportsService = reportsService;
  }

  ngOnInit(): void {
    // console.log(this.reportList);
    // console.log(this.dateStart);
    // console.log(this.dateEnd);
    setTimeout(() => {
      this.repeatFunk();
    }, 60000);
  }

  isBlack(is: boolean): string {
    return is ? 'grey' : '';
  }

  /**
   * запрос на обновление данных
   */
  repeatFunk():void {
    this.updateView();
    setTimeout(() => {
      this.repeatFunk()
    }, 60000);
  }

  updateView(): void {
    let options: HttpParams = new HttpParams();
    const filter = this.filter;
    if (filter.id) {
      options = options.set('id', filter.id.toString());
    }
    if (filter.dateStart && filter.dateStart.isValid()) {
      console.log(filter.dateStart.toJSON());
      options = options.set('dateStart', this.dateStart);
    }
    if (filter.dateEnd && filter.dateEnd.isValid()) {
      options = options.set('dateEnd', this.dateEnd);
    }
    this.reportsService.createReport(options).subscribe(res => {
      console.info('Данные обнавлены');
      this.reportList = res.body;
    }, error => {
      console.error('Произошла ошибка');
    })
  }
}
