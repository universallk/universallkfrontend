import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ThemesComponent} from '../themes/themes.component';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {ReportsComponent} from './reports.component';


const routes: Routes = [
  {
    path: 'reports',
    component: ReportsComponent,
    data: {
      authorities: ['ADMIN', 'USER_IVR', 'SUPER_VISOR'],
    },
    canActivate: [AuthGuardService],
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
