import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkillsRoutingModule } from './skills-routing.module';
import { SkillsComponent } from './skills.component';
import { SkillsDeleteComponent } from './skills-delete/skills-delete.component';
import { SkillsEditComponent } from './skills-edit/skills-edit.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule, NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [SkillsComponent, SkillsDeleteComponent, SkillsEditComponent],
  imports: [
    CommonModule,
    SkillsRoutingModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    NgbModule,
    FormsModule,
  ],
  entryComponents: [SkillsEditComponent, SkillsDeleteComponent]
})
export class SkillsModule { }
