import { Component, OnInit } from '@angular/core';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {EventManagerService} from '../../shared/sevice/event-manager.service';
import {AppsService} from '../apps/apps.service';
import {Theme} from '../../shared/models/theme.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import {IApp} from '../../shared/models/app.model';
import {AuthService} from '../../core/auth/auth.service';
import {EditThemeDialogComponent} from '../themes/edit-theme-dialog/edit-theme-dialog.component';
import {HttpParams} from '@angular/common/http';
import {DeleteThemeComponent} from '../themes/delete-theme/delete-theme.component';
import {SkillsService} from './skills.service';
import {Skill} from '../../shared/models/skill.model';
import {SkillsDeleteComponent} from './skills-delete/skills-delete.component';
import {SkillsEditComponent} from './skills-edit/skills-edit.component';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  eventManagerService: EventManagerService;
  appsService: AppsService;
  skillsService: SkillsService;
  skills: Skill[] = [];
  modalService: NgbModal;

  filterForm = this.fb.group({
    appId: [null, [Validators.required]],
  });
  apps: IApp[] = [];


  constructor(modalService: NgbModal, protected fb: FormBuilder,
              authService: AuthService, appService: AppsService, skillsService: SkillsService) {
    this.appsService = appService;
    this.modalService = modalService;
    this.skillsService =skillsService;
  }

  ngOnInit(): void {
    this.loadApp();
  }

  trackAppById(index: number, item: IApp) {
    return item.id;
  }

  addNewSkill() {
    const modalRef = this.modalService.open(SkillsEditComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.appId = this.filterForm.get(['appId']).value;
    modalRef.componentInstance.init();
    modalRef.result.then(result => {
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  loadApp() {
    let options: HttpParams = new HttpParams();
    options = options.set('isEnabled.equals', 'true');
    options = options.set('appType.equals', 'IVR');
    this.appsService.findAllNotDisabled(options).subscribe(res => {
      this.apps = res.body;
    });
  }

  loadPage(page?: number) {
    const pageToLoad: number = page ? page : this.page;
    console.log(pageToLoad);
    let options: HttpParams = new HttpParams();
    if (pageToLoad !== undefined) {
      options = options.set('page', (pageToLoad - 1).toString());
    }
    options = options.set('size', this.itemsPerPage.toString());
    options = options.set('sort', 'id');

    if (this.filterForm.get(['appId']).value) {
      options = options.set('appId.equals',  this.filterForm.get(['appId']).value);
    }
    this.skillsService.findAll(options).subscribe(res => {
      this.skills = res.body.content;
      this.totalItems = res.body.totalElements;
    });
  }

  edit(skill: Skill) {
    const modalRef = this.modalService.open(SkillsEditComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.skill = skill;
    modalRef.componentInstance.init();
    modalRef.result.then(result => {
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  delete(theme: Theme): void {
    const modalRef = this.modalService.open(SkillsDeleteComponent, {size: 'lg', backdrop: 'static'});
    modalRef.componentInstance.theme = theme;
    modalRef.result.then(result => {
      console.log(result);
      if (result && result.save) {
        this.loadPage();
      }
    });
  }

  changeApp() {
    if (!this.filterForm.invalid && this.filterForm.get(['appId']).value) {
      this.loadPage(1);
    } else {
      this.skills = [];
    }
  }
}
