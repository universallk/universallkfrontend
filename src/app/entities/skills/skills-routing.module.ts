import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {InterviewComponent} from '../interview/interview.component';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {SkillsComponent} from './skills.component';


const routes: Routes = [
  { path: 'skills',
    component: SkillsComponent,
    data: {
      authorities: ['ADMIN'],
    },
    canActivate: [AuthGuardService],
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkillsRoutingModule { }
