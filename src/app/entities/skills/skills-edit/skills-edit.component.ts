import { Component, OnInit } from '@angular/core';
import {Theme} from '../../../shared/models/theme.model';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import {HttpResponse} from '@angular/common/http';
import {IApp} from '../../../shared/models/app.model';
import {SkillsService} from '../skills.service';
import {Skill} from '../../../shared/models/skill.model';

@Component({
  selector: 'app-skills-edit',
  templateUrl: './skills-edit.component.html',
  styleUrls: ['./skills-edit.component.scss']
})
export class SkillsEditComponent implements OnInit {
  appId: number;
  skillsService: SkillsService;
  skill: Skill;
  errorMessage?: string;

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, skillsService: SkillsService) {
    this.skillsService = skillsService;
  }

  editForm = this.fb.group({
    id: [null],
    name: [null, [Validators.required]],
    value: [null, [Validators.required]],
    appId: [null, [Validators.required]],
  });
  ngOnInit(): void {
  }

  clear() {
    this.activeModal.close();
  }
  save() {
    const theme1 = this.updateApp();
    console.log('сохраняем приложение');
    if (!theme1.id) {
      this.skillsService.save(theme1).subscribe(res => {
          this.saveResult(res);
        }
      );
    } else {
      this.skillsService.update(theme1).subscribe(res => {
        this.saveResult(res);
      });
    }
  }

  private saveResult(res: HttpResponse<IApp>) {
    console.log(res.body.id);
    if (res.body && !res.body.errorMessage) {
      this.activeModal.close({
        save: true
      });
    } else if (res.body && res.body.errorMessage){
      this.errorMessage = res.body.errorMessage;
    }
  }

  updateApp(): Theme {
    return {
      ...new Theme(),
      id: this.editForm.get(['id']).value ? this.editForm.get(['id']).value : null,
      name: this.editForm.get(['name']).value,
      value: this.editForm.get(['value']).value,
      appId: this.editForm.get(['appId']).value,
    };
  }

  init() {
    console.log(this.skill);
    if (this.skill) {
      this.editForm.patchValue({
        id: this.skill.id,
        name: this.skill.name,
        value: this.skill.value,
        appId: this.skill.appId,
      });
    } else {
      this.editForm.patchValue({
        appId: this.appId
      });
    }
  }

}
