import { Component, OnInit } from '@angular/core';
import {Theme} from '../../../shared/models/theme.model';
import {ThemeService} from '../../themes/theme.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Skill} from '../../../shared/models/skill.model';
import {SkillsService} from '../skills.service';

@Component({
  selector: 'app-skills-delete',
  templateUrl: './skills-delete.component.html',
  styleUrls: ['./skills-delete.component.scss']
})
export class SkillsDeleteComponent implements OnInit {
  skill?: Skill;
  constructor(
    protected skillService: SkillsService,
    public activeModal: NgbActiveModal,
  ) {
  }

  clear(): void {
    this.activeModal.close();
  }

  confirmDelete(id: number): void {
    this.skillService.delete(id).subscribe(() => {
      this.activeModal.close({
        save: true
      });
    });
  }

  ngOnInit(): void {
  }

}
