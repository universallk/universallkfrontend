import { Injectable } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthService} from '../../core/auth/auth.service';
import {Observable} from 'rxjs';
import {Page} from '../../shared/models/page.model';
import {Skill} from '../../shared/models/skill.model';

@Injectable({
  providedIn: 'root'
})
export class SkillsService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/skills';
  constructor(private http: HttpClient, protected authService: AuthService) { }


  findAll(options: HttpParams): Observable<HttpResponse<Page<Skill>>> {
    return this.http.get<Page<Skill>>(this.url, {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

  findAllNotPage(options: HttpParams): Observable<HttpResponse<Skill[]>> {
    return this.http.get<Skill[]>(this.url + '/non-page', {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }
  save(options: Skill): Observable<HttpResponse<Skill>> {
    return this.http.post<Skill>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  update(options: Skill): Observable<HttpResponse<Skill>> {
    return this.http.put<Skill>(this.url, options,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.url}/${id}`,
      {
        headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
        observe: 'response'
      });
  }
}
