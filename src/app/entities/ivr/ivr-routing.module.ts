import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ReportsComponent} from '../reports/reports.component';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {IvrComponent} from './ivr.component';


const routes: Routes = [
  {
    path: 'appsIvr',
    component: IvrComponent,
    data: {
      authorities: ['ADMIN', 'USER_IVR', 'SUPER_VISOR'],
    },
    canActivate: [AuthGuardService],
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IvrRoutingModule { }
