import { Component, OnInit } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {AppsService} from '../apps/apps.service';
import {IApp} from '../../shared/models/app.model';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {FullAudio} from '../../shared/models/full-audio.model';
import {InterviewService} from '../interview/interview.service';
import {AuthService} from '../../core/auth/auth.service';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {Filter} from '../../shared/models/filter.model';
import {DATE_TIME_FORMAT, DATE_TIME_FORMAT2} from '../../shared/constant/format.constant';
import * as moment from 'moment';
import {SoundMessage} from '../../shared/models/sound-message.model';
import {SOUND_MESSAGES} from '../../shared/constant/sound-messages.constant';
import {Skill} from '../../shared/models/skill.model';
import {Theme} from '../../shared/models/theme.model';
import {ThemeService} from '../themes/theme.service';
import {SkillsService} from '../skills/skills.service';
import {IvrService} from './ivr.service';

@Component({
  selector: 'app-ivr',
  templateUrl: './ivr.component.html',
  styleUrls: ['./ivr.component.scss']
})
export class IvrComponent implements OnInit {
  phoneMask = [ /[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  url: string = GLOBAL_URL + '/api/ivr';
  errorMessage?: string = 'Задайте условия поиска';
  appsService: AppsService;
  apps: IApp[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  fullAudios: FullAudio[] = [];
  ivrService: IvrService;
  authService: AuthService;
  isSpinner = true;
  headers: any;
  themeService: ThemeService;
  skillsService: SkillsService;
   filterCount = 0;

  constructor(private http: HttpClient, appsService: AppsService, protected router: Router ,ivrService: IvrService,
              protected fb: FormBuilder, authService: AuthService,
              themeService: ThemeService, skillsService: SkillsService) {
    this.appsService = appsService;
    this.authService = authService;
    this.ivrService = ivrService;
    this.skillsService = skillsService;
    this.themeService = themeService;
  }

  filterForm = this.fb.group({
    id: [null, [Validators.required]],
    phone: [null],
    dateStart: [null],
    dateEnd: [null],
    theme: [null],
    skill: [null],
    soundMessage: [null],
  });
  soundMessages: SoundMessage[] = SOUND_MESSAGES;
  skills: Skill[] = [];
  themes: Theme[] = [];

  ngOnInit(): void {
    this.initCurrentDateTime();
    this.loadEnabledApp();
  }
  loadEnabledApp() {
    let options: HttpParams = new HttpParams();
    options = options.set('isEnabled.equals', 'true');
    options = options.set('appType.equals', 'IVR');
    this.appsService.findAllNotDisabled(options).subscribe(res => {
      this.apps = res.body;
      console.log(this.apps.length);
    });
  }
  previousState() {
    window.history.back();
  }

  trackById(index: number, item: IApp) {
    return item.id;
  }

  clearMessage() {
    this.errorMessage = null;
  }

  loadThemesByAppId() {
    if (this.filterForm.get(['id']).value) {
  let options: HttpParams = new HttpParams();
  options = options.set('appId.equals', this.filterForm.get(['id']).value);
  this.themeService.findAllNotPage(options).subscribe(res=> {
      this.themes = res.body;
    });
    }
}
  loadSkillByAppId() {
    if (this.filterForm.get(['id']).value) {
     let options: HttpParams = new HttpParams();
     options = options.set('appId.equals', this.filterForm.get(['id']).value);
     this.skillsService.findAllNotPage(options).subscribe(res=> {
      this.skills = res.body;
    });
    }
}
  findFullAudio(page?: number) {
    this.isSpinner = false;
    this.errorMessage = undefined;
    const pageToLoad: number = page ? page : this.page;
    console.log(pageToLoad);
    let options: HttpParams = new HttpParams();
    if (pageToLoad !== undefined) {
      options = options.set('page', (pageToLoad - 1).toString());
    }
    options = options.set('size', this.itemsPerPage.toString());
    options = options.set('sort', 'id');
    options = options.set('countAll', this.filterCount.toString());
    const filter = this.getFilter();
    if (filter.id) {
      options = options.set('id', filter.id.toString());
    }
    if (filter.phone) {
      options = options.set('phone', filter.phone);
    }  if (filter.dateStart && filter.dateStart.isValid()) {
      console.log(filter.dateStart.toJSON());
      options = options.set('dateStart', this.filterForm.get(['dateStart']).value);
    }  if (filter.dateEnd && filter.dateEnd.isValid()) {
      options = options.set('dateEnd', this.filterForm.get(['dateEnd']).value);
    }
    if (filter.soundMessage) {
      options = options.set('soundMessage', this.filterForm.get(['soundMessage']).value);
    }
    if (filter.skill) {
      options = options.set('skill', this.filterForm.get(['skill']).value);
    } if (filter.theme) {
      options = options.set('theme', this.filterForm.get(['theme']).value);
    }
    this.ivrService.findAll(options).subscribe(res=>{
      this.isSpinner = true;
      this.fullAudios = res.body.content;
      this.totalItems = res.body.totalElements;
      this.filterCount = this.totalItems;
      if (!this.fullAudios || this.fullAudios.length == 0) {
        this.errorMessage = 'Аудио записи не найдены, задайте другие условия поиска';
      } else {
        // this.fullAudios.forEach(e=> this.loadAudio(e.recordFile, e.id));
      }
    }, error => {
      this.isSpinner = true;
      this.errorMessage = 'Аудио записи не найдены, задайте другие условия поиска';
    });
  }

  clearFilter() {
    this.filterForm.patchValue({
      id: null,
      phone: null,
      dateStart: moment().startOf('day').format(DATE_TIME_FORMAT2),
      dateEnd: moment().endOf('day').format(DATE_TIME_FORMAT2)
    });
    this.fullAudios = [];
    this.changeCount();
  }
  getFilter(): Filter {
    return {
      ...new Filter(),
      id: this.filterForm.get(['id']).value,
      phone: this.filterForm.get(['phone']).value,
      skill: this.filterForm.get(['skill']).value,
      theme: this.filterForm.get(['theme']).value,
      soundMessage: this.filterForm.get(['soundMessage']).value,
      dateStart: moment(this.filterForm.get(['dateStart']).value, DATE_TIME_FORMAT),
      dateEnd: moment(this.filterForm.get(['dateEnd']).value, DATE_TIME_FORMAT),
    };
  }
  initCurrentDateTime(): void {
    this.filterForm.patchValue({
      dateStart: moment().startOf('day').format(DATE_TIME_FORMAT2),
      dateEnd: moment().endOf('day').format(DATE_TIME_FORMAT2)
    });
  }

  changeApp(): void {
    this.changeCount();
    this.themes = [];
    this.skills = [];
    this.loadThemesByAppId();
    this.loadSkillByAppId();
  }

  changeCount() {
    this.filterCount = 0;
  }
}

