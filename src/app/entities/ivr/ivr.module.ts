import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IvrRoutingModule } from './ivr-routing.module';
import { IvrComponent } from './ivr.component';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {ReactiveFormsModule} from '@angular/forms';
import {TextMaskModule} from 'angular2-text-mask';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [IvrComponent],
    imports: [
        CommonModule,
        IvrRoutingModule,
        NgbPaginationModule,
        ReactiveFormsModule,
        TextMaskModule,
        SharedModule
    ]
})
export class IvrModule { }
