import { Injectable } from '@angular/core';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {AuthService} from '../../core/auth/auth.service';
import {Observable} from 'rxjs';
import {Page} from '../../shared/models/page.model';
import {IApp} from '../../shared/models/app.model';

@Injectable({
  providedIn: 'root'
})
export class InterviewService {
  private rootUrl: string = GLOBAL_URL + '/api';
  private url: string = GLOBAL_URL + '/api/interview';

  constructor(private http: HttpClient, protected authService: AuthService) {
  }

  findAll(options: HttpParams): Observable<HttpResponse<Page<IApp>>> {
    console.log(this.authService.getCurrentToken());
    return this.http.get<Page<IApp>>(this.url, {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }

  loadAudio(options: HttpParams): Observable<HttpResponse<string>>  {
    console.log(this.authService.getCurrentToken());
    return this.http.get<string>(this.url + '/audioPreview', {
      params: options,
      headers: {Authorization: `Bearer ${this.authService.getCurrentToken()}`},
      observe: 'response'
    });
  }
}
