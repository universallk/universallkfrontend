import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, Validators} from '@angular/forms';
import {App, IApp} from '../../shared/models/app.model';
import {AppsService} from '../apps/apps.service';
import {HttpClient, HttpParams} from '@angular/common/http';
import {FullAudio} from '../../shared/models/full-audio.model';
import {Filter} from '../../shared/models/filter.model';
import {InterviewService} from './interview.service';
import {ITEMS_PER_PAGE} from '../../shared/constant/page.constant';
import {DATE_TIME_FORMAT} from '../../shared/constant/format.constant';
import * as moment from 'moment';
import {GLOBAL_URL} from '../../shared/constant/url.constant';
import {AuthService} from '../../core/auth/auth.service';
import {map} from 'rxjs/operators';
import {HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-interview',
  templateUrl: './interview.component.html',
  styleUrls: ['./interview.component.scss']
})
export class InterviewComponent implements OnInit {
  phoneMask = [/[1-9]/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];

  url: string = GLOBAL_URL + '/api/interview';
  errorMessage?: string = 'Задайте условия поиска';
  appsService: AppsService;
  apps: IApp[] = [];
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page = 1;
  fullAudios: FullAudio[] = [];
  interviewService: InterviewService;
  authService: AuthService;
  isSpinner = true;
  headers: any;
  filterCount = 0;

  constructor(private http: HttpClient, appsService: AppsService, protected router: Router, interviewService: InterviewService,
              protected fb: FormBuilder, authService: AuthService) {
    this.appsService = appsService;
    this.authService = authService;
    this.interviewService = interviewService;
  }

  filterForm = this.fb.group({
    id: [null, [Validators.required]],
    phone: [null],
    dateStart: [null],
    dateEnd: [null],
  });

  ngOnInit(): void {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.authService.getCurrentToken()}`
    });
    this.loadEnabledApp();
  }

  loadEnabledApp() {
    let options: HttpParams = new HttpParams();
    options = options.set('isEnabled.equals', 'true');
    options = options.set('appType.equals', 'INTERVIEW');
    this.appsService.findAllNotDisabled(options).subscribe(res => {
      this.apps = res.body;
      console.log(this.apps.length);
    });
  }

  previousState() {
    window.history.back();
  }

  trackAppById(index: number, item: IApp) {
    return item.id;
  }

  clearMessage() {
    this.errorMessage = null;
  }

  findFullAudio(page?: number) {
    this.isSpinner = false;
    this.errorMessage = undefined;
    const pageToLoad: number = page ? page : this.page;
    console.log(pageToLoad);
    let options: HttpParams = new HttpParams();
    if (pageToLoad !== undefined) {
      options = options.set('page', (pageToLoad - 1).toString());
    }
    options = options.set('size', this.itemsPerPage.toString());
    options = options.set('sort', 'id');
    options = options.set('countAll', this.filterCount.toString());
    const filter = this.getFilter();
    if (filter.id) {
      options = options.set('id', filter.id.toString());
    }
    if (filter.phone) {
      options = options.set('phone', filter.phone);
    }
    if (filter.dateStart && filter.dateStart.isValid()) {
      console.log(filter.dateStart.toJSON());
      options = options.set('dateStart', this.filterForm.get(['dateStart']).value);
    }
    if (filter.dateEnd && filter.dateEnd.isValid()) {
      options = options.set('dateEnd', this.filterForm.get(['dateEnd']).value);
    }
    this.interviewService.findAll(options).subscribe(res => {
      this.isSpinner = true;
      this.fullAudios = res.body.content;
      this.totalItems = res.body.totalElements;
      if (!this.fullAudios || this.fullAudios.length == 0) {
        this.errorMessage = 'Аудио записи не найдены, задайте другие условия поиска';
      } else {
        // this.fullAudios.forEach(e=> this.loadAudio(e.recordFile, e.id));
      }
    }, error => {
      this.isSpinner = true;
      this.errorMessage = 'Аудио записи не найдены, задайте другие условия поиска';
    });
  }

//todo: удалить нахрен походу не понадобится
  loadAudio(pathFile: string, idx: number) {
    let options: HttpParams = new HttpParams();
    if (pathFile) {
      options = options.set('pathFile', pathFile);
    }
    console.log(pathFile);
    this.interviewService.loadAudio(options).pipe(
      map(res => {
        return {
          filename: 'audio.wav',
          type: 'audio/wav',
          data: res.body
        };
      })
    )
      .subscribe(res => {
          if (res.data) {
            // console.log('idx = ' + idx.toString());
            console.log(res.data);
            const url = window.URL.createObjectURL(res.data);
            document.getElementById('field-audio' + idx).setAttribute('src', url);
            // audio.controls = true;
            // audio.loop = true;
            // audio.preload = 'none';
            // audio.id = 'field-audio' + idx;
            // audio.src = url;
            // audio.setAttribute('controlsList', 'nodownload');
            // audio.setAttribute('border', 'none');
            // console.log('document.getElementById(idx.toString()) = ' + document.getElementById(idx.toString()))
            // document.getElementById('field-audio' + idx).setAttribute('src' ,url);
            //       document.getElementById('field-audio' + idx).onplay;
          }
        },
        error => {
          console.log('download error:', JSON.stringify(error));
        },
        () => {
          console.log('Completed file download.');
        }
      );
  }


  clearFilter() {
    this.filterForm.patchValue({
      id: null,
      phone: null,
      dateStart: null,
      dateEnd: null,
    });
    this.fullAudios = [];
    this.changeCount();
  }

  getFilter(): Filter {
    return {
      ...new Filter(),
      id: this.filterForm.get(['id']).value,
      phone: this.filterForm.get(['phone']).value,
      dateStart: moment(this.filterForm.get(['dateStart']).value, DATE_TIME_FORMAT),
      dateEnd: moment(this.filterForm.get(['dateEnd']).value, DATE_TIME_FORMAT),
    };
  }

  changeCount() {
    this.filterCount = 0;
  }
}
