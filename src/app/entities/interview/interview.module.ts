import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InterviewRoutingModule } from './interview-routing.module';
import { InterviewComponent } from './interview.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AdminModule} from '../../admin/admin.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {TextMaskModule} from 'angular2-text-mask';
import {NgxMaskModule} from 'ngx-mask';


@NgModule({
  declarations: [InterviewComponent],
  imports: [
    CommonModule,
    InterviewRoutingModule,
    ReactiveFormsModule,
    AdminModule,
    NgbPaginationModule,
    TextMaskModule,
  ]
})
export class InterviewModule { }
