import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UserComponent} from '../user/user.component';
import {AuthGuardService} from '../../core/guard/auth-guard.service';
import {InterviewComponent} from './interview.component';


const routes: Routes = [
  { path: 'appsInterview',
    component: InterviewComponent,
    data: {
      authorities: ['USER', 'ADMIN', 'SUPER_VISOR'],
    },
    canActivate: [AuthGuardService],
    // canActivateChild: [AuthGuardService]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InterviewRoutingModule { }
