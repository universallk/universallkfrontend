import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../shared/models/user-model';
import {FormBuilder, Validators} from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {AuthService} from '../../core/auth/auth.service';

@Component({
  selector: 'app-show-register-user',
  templateUrl: './show-register-user.component.html',
  styleUrls: ['./show-register-user.component.scss']
})
export class ShowRegisterUserComponent implements OnInit {
  @Input() newUser: User;
  @Output()
  eventEmitter: EventEmitter<User> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    console.log(this.newUser);
  }

  login() {
    this.eventEmitter.emit(this.newUser);
  }
}
