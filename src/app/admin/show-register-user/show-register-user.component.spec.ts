import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowRegisterUserComponent } from './show-register-user.component';

describe('ShowRegisterUserComponent', () => {
  let component: ShowRegisterUserComponent;
  let fixture: ComponentFixture<ShowRegisterUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowRegisterUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowRegisterUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
