import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent implements OnInit {
  @Input() errorMessage?: string;
  @Output() closeAlert = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

  clearMessage() {
    this.errorMessage = null;
    this.closeAlert.emit();
  }
}
