import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { ShowRegisterUserComponent } from './show-register-user/show-register-user.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RegistryComponent } from './registry/registry.component';


@NgModule({
  declarations: [ErrorComponent, LoginComponent, ShowRegisterUserComponent, RegistryComponent],
  exports: [
    ErrorComponent
  ],
    imports: [
        CommonModule,
        AdminRoutingModule,
        ReactiveFormsModule,
        FormsModule
    ]
})
export class AdminModule { }
